# Genie

A Javascript based testing framework to make all of your dreams come true.

# Goals

- [ ] parallel test running
- [ ] headless tests in docker
- [ ] visual regression with backstopjs
- [ ] api calls for back-end setup/teardown/validation
- [ ] eslint to enforce best practices
- [ ] cucumber for reuseable test steps
- [ ] working waitForAjax implementation
- [ ] background check to ensure version of chromedriver is compatible with installed version of chrome, Firefox+geckodriver in the future.
- [ ] cross browser:(Firefox, Edge, Chrome) and cross-platform: Windows 10, Mac OS, Linux.
